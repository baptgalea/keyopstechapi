# KeyOpsTechAPI

## Installation

Cloner le projet :

```shell
git clone https://gitlab.com/baptgalea/keyopstechapi.git
cd keyopstechapi
```

Puis déployer les containers : 

```shell
docker-compose up -d
```

Se connecter ensuite au container php : 

```shell
docker-compose exec php bash
```

Installer ensuite les dépendances du projets : 

```bash
composer install
```

Mettre à jour la base de données et charger les fixtures : 

```shell
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load
```

L'application est désormais accessible.

## Accéder aux écrans et tester l'API

Pour SonataAdminBundle :

`http://localhost/admin`

Pour attaquer l'API :

`http://localhost/api/companies`

`http://localhost/api/companies?limit=20&offset=1`

`http://localhost/api/companies/{id}`

`http://localhost/api/users`

`http://localhost/api/users/{id}`

## Pour utiliser PHPUnit

Se connecter au container php : 

```shell
docker-compose exec php bash
```

Créer la database de test avec la commande suivante :

```shell
php bin/console doctrine:database:create --env=test
```

Puis mettre à jour le schema :

```shell
php bin/console doctrine:schema:update --env=test --force
```

Il est ensuite possible d'executer les tests normalement :

```shell
php ./bin/phpunit
```