<?php

namespace Test;

use App\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Faker;

class CompanyRestControllerTest extends WebTestCase
{

    /** @var string $companyName */
    private $companyName;

    protected function setUp(): void
    {
        $this->companyName = 'keyopstech';
        $this->insertCompanies();
    }

    public function testGetAllCompanies()
    {
        $client = static::createClient();

        $client->request('GET', '/api/companies');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testGetCompany()
    {
        $client = static::createClient();

        $client->request('GET', '/api/companies/' . $this->getTestCompany()->getId());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testGetCompaniesPagination()
    {
        $limit = 5;
        $client = static::createClient();

        $client->request('GET', '/api/companies', ['limit' => $limit]);

        $response = json_decode($client->getResponse()->getContent());

        $this->assertLessThanOrEqual($limit, count($response->data));
    }

    /**
     * Création d'un jeu de données de test pour Companies, dont 1 Company connue
     */
    private function insertCompanies()
    {
        $client = static::createClient();

        $entityManager = $client->getContainer()->get('doctrine')->getManager();

        $company = $this->getRandomCompany($this->companyName);
        $entityManager->persist($company);

        for ($i = 0; $i < 50; $i++)
        {
            $company = $this->getRandomCompany();
            $entityManager->persist($company);
        }
        $entityManager->flush();
    }

    /**
     * @param string|null $companyName
     *
     * @return Company
     */
    private function getRandomCompany(string $companyName = null)
    {
        $faker = Faker\Factory::create();
        $company = new Company();
        if ($companyName === null) {
            $company->setName($faker->company);
        } else {
            $company->setName($this->companyName);
        }
        $company->setPhone($faker->phoneNumber);
        $company->setEmail($faker->companyEmail);
        $company->setAddress($faker->address);
        return $company;
    }

    /**
     * @return null|object
     */
    private function getTestCompany()
    {
        $client = static::createClient();
        $entityManager = $client->getContainer()->get('doctrine')->getManager();
        return $entityManager->getRepository(Company::class)->findOneBy(['name' => $this->companyName]);
    }
}
