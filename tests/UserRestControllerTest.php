<?php

namespace Test;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRestControllerTest extends WebTestCase
{

    /** @var string $usernameTest */
    private $usernameTest;

    protected function setUp(): void
    {
        $this->usernameTest = 'baptistegalea';
        $this->insertUser($this->usernameTest);
    }

    public function testGetAllUsers()
    {
        $client = static::createClient();

        $client->request('GET', '/api/users');

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'Le code status renvoyé est ok');
    }

    public function testGetUserNotHasIdKey()
    {
        $client = static::createClient();

        $client->request('GET', '/api/users/' . $this->getTestUser()->getId());

        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode(), 'Le code status renvoyé est ok');
        $content = json_decode($response->getContent());
        $this->assertObjectNotHasAttribute('id', $content, 'L\'id n\'est pas exposé');
    }

    /**
     * @param string $username
     */
    private function insertUser($username)
    {
        $client = static::createClient();

        $entityManager = $client->getContainer()->get('doctrine')->getManager();

        $user = new User();
        $user->setUsername($username);
        $user->setEmail('test@test.com');
        $user->setFirstname('myfirstname');
        $user->setLastname('mylastname');
        $user->setPassword('123456');
        $entityManager->persist($user);
        $entityManager->flush();
    }

    /**
     * @return null|object
     */
    private function getTestUser()
    {
        $client = static::createClient();
        $entityManager = $client->getContainer()->get('doctrine')->getManager();
        return $entityManager->getRepository(User::class)->findOneBy(['username' => $this->usernameTest]);
    }
}
