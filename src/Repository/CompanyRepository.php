<?php

namespace App\Repository;

use App\Entity\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\LogicException;
use Pagerfanta\Pagerfanta;

class CompanyRepository extends ServiceEntityRepository
{
    /**
     * CompanyRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Company::class);
    }

    public function getUsersQb()
    {
        $qb = $this
            ->createQueryBuilder('company')
            ->select('company')
        ;
        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param int          $limit
     *
     * @param int          $offset
     *
     * @return Pagerfanta
     */
    public function paginate(QueryBuilder $qb, $limit = 20, $offset = 1)
    {
        if (0 == $limit || 0 == $offset)
        {
            throw new LogicException('limit and offset must be greater than 0');
        }

        $ormAdapter = new DoctrineORMAdapter($qb);
        $pager = new Pagerfanta($ormAdapter);

        $currentPage = ceil(($offset) / $limit);

        $pager->setMaxPerPage((int) $limit);
        $pager->setCurrentPage($currentPage);

        return $pager;
    }
}
