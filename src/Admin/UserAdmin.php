<?php

namespace App\Admin;

use App\Entity\Company;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('email', TextType::class)
            ->add('username', TextType::class)
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('password', TextType::class)
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'required' => false,
                'choice_label' => 'name'
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

        $datagridMapper
            ->add('email')
            ->add('username')
            ->add('firstname')
            ->add('lastname')
            ->add('company', null, [], EntityType::class, [
                'class' => Company::class,
                'choice_label' => 'name',
            ])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('email')
            ->addIdentifier('username')
            ->addIdentifier('firstname')
            ->addIdentifier('lastname')
            ->addIdentifier('company.name')
        ;
    }
}
