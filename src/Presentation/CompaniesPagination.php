<?php

namespace App\Presentation;

use Pagerfanta\Pagerfanta;

class CompaniesPagination
{

    private $data;

    private $meta;

    public function __construct(Pagerfanta $pager)
    {
        $this->data = (array) $pager->getCurrentPageResults();

        $this->addMeta('limit', $pager->getMaxPerPage());
        $this->addMeta('offset', $pager->getCurrentPageOffsetStart());
        $this->addMeta('nb_current_items', count($pager->getCurrentPageResults()));
        $this->addMeta('nb_total_items', $pager->getNbResults());

        if ($pager->hasPreviousPage()) {
            $previousPage = $pager->getPreviousPage();
        } else {
            $previousPage = null;
        }

        if ($pager->hasNextPage()) {
            $nextPage = $pager->getNextPage();
        } else {
            $nextPage = null;
        }

        $pagination = [
            'previous_page' => $previousPage,
            'current_page' => $pager->getCurrentPage(),
            'next_page' => $nextPage,
        ];

        $this->addMeta('pagination', $pagination);
    }

    /***
     * @param $key
     * @param $value
     */
    public function addMeta($key, $value)
    {
        $this->meta[$key] = $value;
    }
}

