<?php

namespace App\Manager;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;

abstract class AbstractManager
{

    /** @var ServiceEntityRepositoryInterface $repository */
    protected $repository;

    protected function getRepository()
    {
        return $this->repository;
    }

}
