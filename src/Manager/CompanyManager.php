<?php

namespace App\Manager;

use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityNotFoundException;

class CompanyManager extends AbstractManager
{
    /**
     * CompanyManager constructor.
     *
     * @param CompanyRepository $repository
     */
    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $id
     *
     * @return null|object
     * @throws EntityNotFoundException
     */
    public function getCompany(int $id)
    {
        $company = $this->getRepository()->find($id);

        if ($company === null) {
            throw new EntityNotFoundException('There is no company for this key');
        }
        return $company;
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @
     * @return \Pagerfanta\Pagerfanta
     */
    public function getCompaniesPager($limit = 20, $offset = 1)
    {
        /** @var CompanyRepository $repository */
        $repository = $this->getRepository();
        $qb = $repository->getUsersQb();

        return $repository->paginate($qb, $limit, $offset);
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return array|\Traversable
     */
    public function getCompanies($limit = 20, $offset = 1)
    {
        $pager = $this->getCompaniesPager($limit, $offset);

        return (array) $pager->getCurrentPageResults();
    }
}
