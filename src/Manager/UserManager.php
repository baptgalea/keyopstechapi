<?php

namespace App\Manager;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityNotFoundException;

class UserManager extends AbstractManager
{

    /**
     * UserManager constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $id
     *
     * @return null|object
     * @throws EntityNotFoundException
     */
    public function getUser(int $id)
    {
        $user = $this->getRepository()->find($id);
        if ($user === null) {
            throw new EntityNotFoundException('There is no user for this key');
        }

        return $user;
    }

    public function getUsers()
    {
        return $this->getRepository()->findAll();
    }
}
