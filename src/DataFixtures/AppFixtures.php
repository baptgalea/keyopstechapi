<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        // Company fixtures
        $arrayCompany = [];
        $nbToInsert = 50;
        for ($i = 0; $i < $nbToInsert; $i++)
        {
            $company = new Company();
            $manager->persist($company);
            $company->setName($faker->company);
            $company->setPhone($faker->phoneNumber);
            $company->setEmail($faker->companyEmail);
            $company->setAddress($faker->address);
            $arrayCompany[] = $company;
        }

        // User fixtures
        for ($i = 0; $i < 500; $i++)
        {
            $user = new User();
            $manager->persist($user);
            $user->setEmail($faker->email);
            $user->setUsername($faker->userName);
            $user->setFirstname($faker->firstName);
            $user->setLastname($faker->lastName);

            /** @var Company $company */
            $company = $arrayCompany[rand(0, $nbToInsert - 1)];
            $user->setCompany($company);
        }

        $manager->flush();
    }
}
