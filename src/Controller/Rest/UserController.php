<?php

namespace App\Controller\Rest;

use App\Manager\UserManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractFOSRestController
{

    /**
     * @Rest\Get("/users")
     * @param UserManager $userManager
     *
     * @return View
     */
    public function getUsersAction(UserManager $userManager): View
    {
        $users = $userManager->getUsers();

        return $this->view($users, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/users/{userId}")
     * @param int         $userId
     *
     * @param UserManager $userManager
     *
     * @return View
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function getUserAction(int $userId, UserManager $userManager): View
    {
        $user = $userManager->getUser($userId);

        return $this->view($user, Response::HTTP_OK);
    }
}
