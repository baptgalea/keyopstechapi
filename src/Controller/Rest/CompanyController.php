<?php

namespace App\Controller\Rest;

use App\Manager\CompanyManager;
use App\Presentation\CompaniesPagination;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

class CompanyController extends AbstractFOSRestController
{

    /**
     * @Rest\Get("/companies")
     * @Rest\QueryParam(
     *     name="limit",
     *     requirements="\d+",
     *     default="20",
     *     description="Max number of companies per page."
     * )
     * @Rest\QueryParam(
     *     name="offset",
     *     requirements="\d+",
     *     default="1",
     *     description="Max number of movies per page."
     * )
     * @param ParamFetcherInterface $paramFetcher
     * @param CompanyManager        $companyManager
     *
     * @return View
     */
    public function getCompaniesAction(ParamFetcherInterface $paramFetcher, CompanyManager $companyManager): View
    {
        $limit = $paramFetcher->get('limit');
        $offset = $paramFetcher->get('offset');
        $companyPager = $companyManager->getCompaniesPager($limit, $offset);

        $companyPagination = new CompaniesPagination($companyPager);

        return View::create($companyPagination, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/companies/{companyId}")
     * @param int            $companyId
     *
     * @param CompanyManager $companyManager
     *
     * @return View
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function getCompanyAction(int $companyId, CompanyManager $companyManager): View
    {
        $company = $companyManager->getCompany($companyId);

        return View::create($company, Response::HTTP_OK);
    }

}
